#!/usr/bin/env python3

import subprocess
import os

HELM_REPO_NAME = 'gitlab'
HELM_REPO_URL  = 'https://charts.gitlab.io'

out = subprocess.run(["helm", "repo", "list"], stdout=subprocess.PIPE, text=True).stdout
out_lines = out.split("\n")


def repo_enabled(helm_stdout):
    return HELM_REPO_NAME in [repo.split()[0] for repo in helm_stdout[1:] if len(repo.split()) > 1]


if len(out_lines) < 2 or not repo_enabled(out_lines):
    subprocess.run(["helm", "repo", "add", HELM_REPO_NAME, HELM_REPO_URL])

os.system("helm upgrade --install gitlab-runner " + HELM_REPO_NAME + "/gitlab-runner --values values.yaml --wait --namespace gitlab-runner --create-namespace ")
