package main

import (
	"net/http"
)
import "encoding/json"

type Request struct {
	Method string
	Body   string
}

func handler(w http.ResponseWriter, req *http.Request) {
	method := req.Method
	body := ""
	if method == "" || method == "GET" {
		method = "GET"
		msg_key, _ := req.URL.Query()["message"]
		if len(msg_key) > 0 {
			body = msg_key[0]
		}
	} else {
		_ = req.ParseForm()
	    body = req.FormValue("message")
	}

	r := &Request{
		Method: method,
		Body:   body,
	}
	j, _ := json.Marshal(r)
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Write(j)
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}
