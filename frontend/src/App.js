import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import ResponseArea from './ResponseArea';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';


export default function App() {
  const backendUrl = process.env.REACT_APP_BACKEND_URL;
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState("Hello World");
  const [method, setMethod] = React.useState("GET");
  const [response, setResponse] = React.useState("");

  const handleMethodChange = (event) => {
    setMethod(event.target.value);
  };

  const handleMessageChange = (event) => {
    setMessage(event.target.value);
  }

  const handleResponse = (data) => {
    setOpen(true);
    const message = "Response: Method " + data.Method + ", Body “" + data.Body +"”"
    setResponse(message);

  }

  const handleRequest = () => {
    const requestOptions = {
              method: method
    };
    const fetch = require('node-fetch');
    if (method == "GET" || method == "DELETE" || method == "") {
        const prms = new URLSearchParams({
            message: message
        });
        fetch(backendUrl + "?" + prms, requestOptions).then(response => response.json()).then(data => handleResponse(data));
    } else {
        fetch(backendUrl, {
            method: method,
            body: new URLSearchParams({
                              message: message
                          }),
        }).then(response => response.json()).then(data => handleResponse(data));
    }

  }

  return (
    <Container maxWidth="sm">
      <Box my={4}>
              <Typography variant="h4" component="h1" gutterBottom>
                Send HTTP request
              </Typography>
              <ResponseArea open={open} setOpen={ setOpen } response={response} />

      </Box>
      <Box my={5}>
      <InputLabel id="label">Method</InputLabel>
      <Select labelId="label" id="select" value={method} onChange={handleMethodChange}>
        <MenuItem value="GET" selected>GET</MenuItem>
        <MenuItem value="POST">POST</MenuItem>
        <MenuItem value="PUT">PUT</MenuItem>
        <MenuItem value="DELETE">DELETE</MenuItem>
        <MenuItem value="PATCH">PATCH</MenuItem>
      </Select>

      </Box>
      <Box>
      <InputLabel id="label">Body</InputLabel>
              <TextField required id="standard-required" onChange={handleMessageChange} defaultValue={message} />
      </Box>
      <Box my={4}>
      <Button
                      variant="outlined"
                      onClick={handleRequest}
                    >
                      Send
                    </Button>
      </Box>

    </Container>
  );
}
